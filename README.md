# Muvi Pipeline for  Helm Deployment on VirtualBox

This Pipeline is based on helm deployment using self runner(Virtualbox)

Pre-requisite:
    
    Runner configurations:
        1. linux(Debian,wsl,Redhat,windows(Not Support))
        2. gitlab-runner setup
        3. Docker
        4. jq

GitLab Project Level Variable:


    You have to add global protected variable for docker login. 
    if not, please add below variable under settings-->CICD-->Variable
        
        CI_REGISTRY_USR - <username>
        CI_REGISTRY_PASSWD - <password>

.gitlab-ci.yml file update:

    Before running pipeline, you have to pass below variable as mandatory.

        1. REPO_NAME: <Image_Name>
        2. CONTAINERPORT: <Container_Port>
        3. APP_VERSION: <Application_version>
        4. CHART_VERSION: <Helm_Chart_version>

    For your reference:-
    
        include:
      - project: 'muvi-pipeline/development'
        file: 'muvi-pipeline.gitlab-ci.yml'
        ref:  'V2.LATEST' 
    default:
      tags:
        - dev-runners
    variables:
      APP_NAME: abcde
      CONTAINERPORT: 80
      APP_VERSION: v3.2.1
      CHART_VERSION: v1.2.3

Branch strategy:

        1. Our Pipeline will support all the branch (dev, stage, test, feature, master,main i.e...). Each deployment based on the branch name.
        2. Test, Master and Main branches are protected with manual trigger.


Stages:
    
    Our Pipeline will perform below five stages
        1. validate
        2. build
        3. test(Manual Trigger)
        4. deploy(Manual trigger)
        5. destroy(Manual Trigger)
    deploy stage will run only on master/main branch. If you are running code under development or any feature, branch. You have to create MR from source branch to master/main for deploy stage.

Job Results:-  
  
    You can check your results on test stage itself, Once you're satisfied then you can go with deploy stage.

 Destroy:

    We have a destroy jobs for docker and helm. 

                        
************THANK YOU************
